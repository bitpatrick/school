package school;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import javax.swing.Spring;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import it.spring.project.dto.StudenteDTO;


class AppTest {

	@Test
	void test() {
		
		StudenteDTO studenteDTO = new StudenteDTO(1, "Mario", "Rossi", LocalDate.now());
		
		assertEquals("Mario", studenteDTO.getNome());
		
		
		
	}

}
