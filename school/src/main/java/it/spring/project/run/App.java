package it.spring.project.run;

import java.time.LocalDate;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import it.spring.project.config.AppConfig;
import it.spring.project.config.PersistenceJPAConfig;
import it.spring.project.dto.StudenteDTO;
import it.spring.project.service.StudenteService;

public class App {

	public static void main(String[] args) {

		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class,
				PersistenceJPAConfig.class);

		StudenteService studenteService = ctx.getBean(StudenteService.class);

		StudenteDTO studenteDTO1 = new StudenteDTO(1, "Patrizio", "Cervigni", LocalDate.of(1987, 10, 19));
		
		Integer matricolaStudenteSalvato = studenteService
				.salva(studenteDTO1);

		StudenteDTO studenteDTO2 = studenteService.recupera(matricolaStudenteSalvato);

		System.out.println(studenteDTO1 == studenteDTO2);

		ctx.close();
	}

}
