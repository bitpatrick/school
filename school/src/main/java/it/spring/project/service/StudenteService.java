package it.spring.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.spring.project.dto.StudenteDTO;
import it.spring.project.model.SchoolRepository;
import it.spring.project.model.StudenteModel;

@Service
public class StudenteService {

	@Autowired
	private SchoolRepository<StudenteDTO, Integer> studentiRepository;

	public Integer salva(StudenteDTO studente) {

		return this.studentiRepository.salva(studente);

	}
	
	public StudenteDTO recupera(Integer matricola) {
		
		return this.studentiRepository.recupera(matricola);
	}

}
