package it.spring.project.persistence.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "studenti")
public class StudenteEntity {

	@Id
	private Integer matricola;
	private String nome;
	private String cognome;
	private LocalDate dataDiNascita;

}
