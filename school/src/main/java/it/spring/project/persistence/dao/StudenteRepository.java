package it.spring.project.persistence.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.spring.project.dto.StudenteDTO;
import it.spring.project.model.SchoolRepository;
import it.spring.project.persistence.entity.StudenteEntity;

@Repository
public interface StudenteRepository
		extends SchoolRepository<StudenteDTO, Integer>, JpaRepository<StudenteEntity, Integer> {

	@Override
	default Integer salva(StudenteDTO studente) {

		StudenteEntity studenteEntity = new StudenteEntity();

		studenteEntity.setMatricola(studente.getMatricola());
		studenteEntity.setNome(studente.getNome());
		studenteEntity.setCognome(studente.getCognome());
		studenteEntity.setDataDiNascita(studente.getDataDiNascita());

		return this.save(studenteEntity).getMatricola();

	}

	@Override
	default StudenteDTO recupera(Integer matricola) {

		return this.findByMatricola(matricola);

	}

	StudenteDTO findByMatricola(Integer matricola);

}
