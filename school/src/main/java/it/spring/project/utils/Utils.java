package it.spring.project.utils;

public class Utils {

	public static int generaNumeroCasuale(int max, int min) {

		return (int) Math.floor(Math.random() * (max - min + 1) + min);

	}

	public static char generaLetteraCasuale() {

		int randomNumber = generaNumeroCasuale(65, 90);

		return (char) randomNumber;
	}

}
