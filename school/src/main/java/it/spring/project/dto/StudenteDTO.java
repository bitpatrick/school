package it.spring.project.dto;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class StudenteDTO {
	
	private Integer matricola;
	private String nome;
	private String cognome;
	private LocalDate dataDiNascita;

}
