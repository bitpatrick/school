package it.spring.project.config;

import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import it.spring.project.model.Classe;
import it.spring.project.model.StudenteModel;
import it.spring.project.utils.Utils;

/**
 * Spring Java Based Configuration
 */
@ComponentScan({ "it.spring.project.model" , "it.spring.project.service" })
@Configuration
public class AppConfig {

	@Bean
	public Classe classe(Set<StudenteModel> studenti) {
		return new Classe(Utils.generaLetteraCasuale(), studenti);
	}

	@Bean
	public Set<StudenteModel> studenti() {

		return Stream
				.of(new StudenteModel(1, "Mario", "Rossi", LocalDate.of(1990, 10, 10)),
						new StudenteModel(2, "Luigi", "Verdi", LocalDate.of(1989, 9, 9)),
						new StudenteModel(3, "Maria", "Gialli", LocalDate.of(1998, 8, 8)),
						new StudenteModel(4, "Francesco", "Neri", LocalDate.of(1987, 10, 9)),
						new StudenteModel(5, "Davide", "Blu", LocalDate.of(1985, 7, 7)))
				.collect(Collectors.toSet());

	}

	@Bean
	public String nome() {
		return "nome";
	}

	@Bean
	public String cognome() {
		return "cognome";
	}

	@Bean
	public LocalDate dataOdierna() {
		return LocalDate.now();
	}
	

}
