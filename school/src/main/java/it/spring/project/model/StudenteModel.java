package it.spring.project.model;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@EqualsAndHashCode(of = { "matricola" })
@ToString
public class StudenteModel {

	private Integer matricola;
	private String nome;
	private String cognome;
	private LocalDate dataDiNascita;

}
