package it.spring.project.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;


@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public class Id {
	
	private Integer codice;
	
	public static Id crea(Integer codice) {
		
		checkPatternId(codice);
		
		return new Id(codice);
		
	}
	
	private static void checkPatternId(Integer codice)  {
		
		if ( codice <= 0 ) {
			
			throw new IllegalArgumentException();
		}
		
	}

}
