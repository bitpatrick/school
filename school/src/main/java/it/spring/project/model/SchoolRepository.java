package it.spring.project.model;

public interface SchoolRepository<T, K> {
	
	Integer salva(T input);
	
	T recupera(K chiave);

}
