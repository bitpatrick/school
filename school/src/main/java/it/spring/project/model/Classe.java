package it.spring.project.model;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class Classe {

	private Character sezione;
	private Set<StudenteModel> studenti;

	public boolean rimuoviStudente(StudenteModel studente) {

		return this.studenti.remove(studente);

	}

	public boolean aggiungiStudente(StudenteModel studente) {

		return this.studenti.add(studente);

	}

}
